<?php

/**
 * @file
 * Administration pages for the authtoken module.
 */

/**
 * Build a form which invalidates authentication tokens by authtoken type.
 */
function authtoken_invalidate_by_type_form($form, &$form_state) {
  if (!empty($form_state['values']['confirm_delete'])) {
    return authtoken_invalidate_confirm_form($form, 'admin/config/system/authtoken/types');
  }
  $form_state['redirect'] = 'admin/config/system/authtoken/types';

  $issued_types = authtoken_get_issued_authtoken_types();
  $issued_types = array_map('check_plain', $issued_types);

  $authtoken_types = array();
  $authtoken_types['all'] = t('All authentication token types');

  foreach ($issued_types as $issued_type) {
    if (($authtoken_type_info = authtoken_get_info($issued_type)) && !empty($authtoken_type_info['label'])) {
      $authtoken_types[$issued_type] = check_plain($authtoken_type_info['label']) . ' (' . $issued_type . ')';
    }
    else {
      $authtoken_types[$issued_type] = $issued_type;
    }
  }

  $form['authtoken_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Invalidate authentication tokens for the following types:'),
    '#options' => $authtoken_types,
    '#default_value' => !empty($form_state['values']['authtoken_types']) ? $form_state['values']['authtoken_types'] : array(),
    '#description' => t('The authentication tokens issued for the selected types will be invalidated.'),
    '#multiple' => TRUE,
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Invalidate'),
    '#submit' => array('authtoken_invalidate_by_type_form_submit'),
  );

  return $form;
}

/**
 * Form submit callback for authtoken_invalidate_by_type_form().
 */
function authtoken_invalidate_by_type_form_submit($form, &$form_state) {
  if (!isset($form_state['values']['confirm'])) {
    $form_state['rebuild'] = TRUE;
    $form_state['storage'] = $form_state['values'];
    $form_state['values']['confirm_delete'] = TRUE;
    return;
  }

  if ($form_state['values']['confirm']) {
    $authtoken_types = array_filter($form_state['storage']['authtoken_types']);

    // All authentication tokens.
    if (isset($authtoken_types['all'])) {
      $num = authtoken_delete();
    }
    // Checked authtoken types.
    else {
      $num = authtoken_delete_by_type($authtoken_types);
    }

    if ($num > 0) {
      drupal_set_message(format_plural($num, 'Invalidated 1 authentication token.', 'Invalidated @count authentication tokens.'));
    }
    else {
      drupal_set_message(t('No authentication tokens were invalidated.'));
    }
  }
}

/**
 * Build a form which invalidates authentication tokens by user role.
 */
function authtoken_invalidate_by_user_role_form($form, &$form_state, $authtoken_types, $redirect) {
  if (empty($authtoken_types)) {
    return array();
  }

  if (!empty($form_state['values']['confirm_delete'])) {
    return authtoken_invalidate_confirm_form($form, $redirect);
  }
  $form_state['redirect'] = $redirect;

  if (!isset($form_state['#authtoken_types'])) {
    $form_state['#authtoken_types'] = $authtoken_types;
  }

  $user_roles = array_map('check_plain', user_roles(TRUE));
  $user_roles[DRUPAL_AUTHENTICATED_RID] = t('All roles');

  $form['user_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Invalidate authentication tokens for the following user roles:'),
    '#options' => $user_roles,
    '#default_value' => !empty($form_state['values']['user_roles']) ? $form_state['values']['user_roles'] : array(),
    '#description' => t('The authentication tokens issued to members of the selected roles will be invalidated.'),
    '#multiple' => TRUE,
    '#required' => TRUE,
  );

  $form['user_roles_any'] = array(
    '#type' => 'checkbox',
    '#title' => t('In case users have multiple roles, enable this if their authentication tokens should be invalidated even if one of their role is not selected.'),
    '#default_value' => !empty($form_state['values']['user_roles_any']) ? $form_state['values']['user_roles_any'] : FALSE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Invalidate'),
    '#submit' => array('authtoken_invalidate_by_user_role_form_submit'),
  );

  return $form;
}

/**
 * Form submit callback for authtoken_invalidate_by_user_role_form().
 */
function authtoken_invalidate_by_user_role_form_submit($form, &$form_state) {
  if (empty($form_state['#authtoken_types'])) {
    drupal_set_message(t('No authentication tokens were invalidated.'));
    return;
  }
  $authtoken_types = $form_state['#authtoken_types'];

  if (!isset($form_state['values']['confirm'])) {
    $form_state['rebuild'] = TRUE;
    $form_state['storage'] = $form_state['values'];
    $form_state['values']['confirm_delete'] = TRUE;
    return;
  }

  if ($form_state['values']['confirm']) {
    $user_roles = $form_state['storage']['user_roles'];
    $user_roles_any = $form_state['storage']['user_roles_any'];

    // All authentication tokens.
    if (!empty($user_roles[DRUPAL_AUTHENTICATED_RID])) {
      $num = authtoken_delete_by_type($authtoken_types);
    }
    // Checked roles.
    else {
      $checked_roles = array_filter($user_roles);
      $checked_roles = array_keys($checked_roles);

      $num = authtoken_delete_by_user_role($authtoken_types, $checked_roles, $user_roles_any);
    }

    if ($num > 0) {
      drupal_set_message(format_plural($num, 'Invalidated 1 authentication token.', 'Invalidated @count authentication tokens.'));
    }
    else {
      drupal_set_message(t('No authentication tokens were invalidated.'));
    }
  }
}

/**
 * Build a confirm form for invalidate forms.
 *
 * @see authtoken_invalidate_by_type_form()
 * @see authtoken_invalidate_by_user_role_form()
 */
function authtoken_invalidate_confirm_form($form, $redirect) {
  return confirm_form(
    $form,
    t('Are you sure you want to invalidate the authentication tokens associated to the selected entries?'),
    $redirect,
    NULL,
    t('Invalidate authentication tokens'),
    t('Cancel')
  );
}
