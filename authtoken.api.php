<?php

/**
 * @file
 * Hooks provided by the authtoken module.
 */

/**
 * This hook allows to define authentication token types.
 *
 * An authtoken type defines how an authentication token should be generated
 * for an entity type.
 *
 * @return array
 *   An array keyed by authtoken type machine name, each value is an array
 *   containing the following elements:
 *   - 'entity type': The machine name of the entity type for which the
 *     authtoken type applies.
 *   - 'label': (optional) The label of the authtoken type to appear in
 *     administration pages.
 *   - 'settings': (optional) An array of settings with the following elements
 *     + 'max error count': (optional) The number of times authentication token
 *       errors are processed. Defaults to AUTHTOKEN_MAX_ERROR_COUNT.
 *     + 'max usage count': (optional) The number of times an authentication
 *       token can be used. A value of zero sets no limit.
 *       Defaults to AUTHTOKEN_MAX_USAGE_COUNT.
 *     + 'threshold expire': (optional) The number of seconds after which
 *       authentication tokens are expired. Defaults to
 *       AUTHTOKEN_THRESHOLD_EXPIRE.
 *     + 'threshold delete': (optional) The number of seconds after which
 *       authentication tokens are deleted. A value of zero disables deletions.
 *       Defaults to AUTHTOKEN_THRESHOLD_DELETE.
 *   - 'generate token callback': The name of a function implementing
 *     callback_authtoken_type_generate_token().
 *   - 'validate callbacks': (optional) An array keyed by error code, each
 *     value is the name of a function implementing
 *     callback_authtoken_type_validate(). An error code is a string prefixed
 *     by the module name. Defaults to the validation callbacks associated to
 *     the error codes AUTHTOKEN_INVALID_MAX_ERROR, AUTHTOKEN_INVALID_EXPIRED,
 *     AUTHTOKEN_INVALID_MAX_USAGE, AUTHTOKEN_INVALID_ACCOUNT.
 *     See authtoken_validate().
 */
function hook_authtoken_type_info() {
  return array(
    'my_authtoken_type' => array(
      'entity type' => 'my_entity_type',
      'label' => 'My authentication token type',
      'settings' => array(
        'max error count'  => 3,
        'max usage count'  => 5,
        'threshold expire' => 60 * 60 * 24 * 7,
        'threshold delete' => 60 * 60 * 24 * 30,
      ),
      'generate token callback' => 'my_module_authtoken_type_generate_token',
      'validate callbacks' => array(
        'my_module_authtoken_invalid_expiration' => 'my_validate_expiration_callback',
        'my_module_authtoken_invalid_account'    => 'my_validate_account_callback',
      ),
    ),
  );
}

/**
 * This hook allows to alter the authtoken types defined by other modules.
 *
 * @param array $types_info
 *   An alterable array representing the authtoken types.
 *
 * @see hook_authtoken_type_info()
 */
function hook_authtoken_type_info_alter(&$types_info) {
  $types_info['another_authtoken_type']['settings']['max usage count'] = 1;
}

/**
 * This callback defines how to generate a token.
 *
 * @param string $authtoken_type
 *   The authentication token type.
 * @param string $account
 *   The account.
 * @param string $entity_type
 *   The entity type.
 * @param string $entity
 *   The entity.
 *
 * @return string|false
 *   Returns an unique token or FALSE.
 */
function callback_authtoken_type_generate_token($authtoken_type, $account, $entity_type, $entity) {
  return drupal_strtoupper(bin2hex(drupal_random_bytes(16)));
}
