Authtoken
=========

This module is aimed at developers and provides the structure required
to generate, store, validate and invalidate expiring authentication tokens.
An authentication token is issued for an account and has a target entity.

Typically, the account defines /who/ the authentication token is issued to and
the target entity /what/ it can be used for.

This module makes no assumption about how the token should be built except that
it must be unique.

The Mailreply module is an example practical use-case.
