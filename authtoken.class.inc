<?php

/**
 * @file
 * Authentication token class.
 */

/**
 * Authentication token class.
 */
class Authtoken {

  protected $token;

  protected $uid;

  protected $authtoken_type;

  protected $entity_type;

  protected $entity_id;

  protected $usage_count;

  protected $error_count;

  protected $created;

  protected $updated;

  /**
   * Constructs a read-only authentication token.
   */
  public function __construct(array $values) {
    foreach ($values as $key => $value) {
      $this->{$key} = $value;
    }
  }

  /**
   * Get the token.
   */
  public function token() {
    return $this->token;
  }

  /**
   * Get the authentication token type.
   */
  public function type() {
    return $this->authtoken_type;
  }

  /**
   * Get the user UID for which the authentication token is issued.
   */
  public function uid() {
    return $this->uid;
  }

  /**
   * Get the entity type associated to the authentication token.
   */
  public function entity_type() {
    return $this->entity_type;
  }

  /**
   * Get the ID of the entity for which the authentication token is issued.
   */
  public function entity_id() {
    return $this->entity_id;
  }

  /**
   * Get the usage count.
   */
  public function usage_count() {
    return $this->usage_count;
  }

  /**
   * Get the error count.
   */
  public function error_count() {
    return $this->error_count;
  }

  /**
   * Get the created date as timestamp.
   */
  public function created() {
    return $this->created;
  }

  /**
   * Get the updated date as timestamp.
   */
  public function updated() {
    return $this->updated;
  }

}
